{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook analyses the DE Shaw dataset \"DESRES 100 µs MD of 3CLpro, no water or ions (100 µs )\" (see https://covid.molssi.org//simulations/#sars-cov-2-main-protease-3clpro-or-nsp5) and extracts a reasonably-sized but diverse subset of structures of the catalytic domain that may be useful for, e.g., docking studies.\n",
    "\n",
    "Structures (\"waymarks\") are picked sequentially from both A chain and B chain copies of the catalytic domain in each snapshot, if their minimum rmsd from any previously selected structure exceeds a user-defined threshold. These waymarks are then used to produce a Voronoi partitioning of the trajectory. Waymarks defining domains that include more than a set minimum fraction of the trajectory snapshots are then output in PDB format.\n",
    "\n",
    "Citation for the trajectory data:\n",
    "\n",
    "    D. E. Shaw Research, \"Molecular Dynamics Simulations Related to\n",
    "    SARS-CoV-2,\" D. E. Shaw Research Technical Data, 2020.\n",
    "    http://www.deshawresearch.com/resources_sarscov2.html/\n",
    "\n",
    "\n",
    "Author: [Charlie Laughton](mailto:charles.laughton@nottingham.ac.uk) School of Pharmacy and Biodiscovery Institute, University of Nottingham, UK."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mdtraj as mdt\n",
    "import numpy as np\n",
    "import glob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parameters defining how the structure selection is done.\n",
    "\n",
    "1. cd_selection: the selection string (MDTraj syntax) for the catalytic domain.\n",
    "2. rmsd_thresh: the minimum rmsd (in nanometers) between waymark structures.\n",
    "3. percent_cut: minimum percentage of the trajectory snapshots associated with a waymark for that waymark to be output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd_selection = 'resid 9 to 194'\n",
    "rmsd_thresh = 0.2\n",
    "percent_cut = 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download and unpack the DE Shaw trajectories if required:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#! curl https://covmme.molssi.org/data/desres/desres_trajs_0/main_protease/DESRES-Trajectory_sarscov2-10880334-no-water-no-ion-glueCA.tar --output DESRES-Trajectory_sarscov2-10880334-no-water-no-ion-glueCA.tar\n",
    "#! tar -xvf DESRES-Trajectory_sarscov2-10880334-no-water-no-ion-glueCA.tar"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the trajectory files, stripping down to heavy atoms. This cell may take a couple of minutes to run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "system = mdt.load('system.pdb')\n",
    "protein = system.topology.select('protein')\n",
    "protop = system.topology.subset(protein)\n",
    "heavy_atoms = protop.select('mass > 1.5')\n",
    "dcdfiles = glob.glob('sarscov2-10880334-no-water-no-ion-glueCA/sarscov2-10880334-no-water-no-ion-glueCA-????.dcd')\n",
    "dcdfiles.sort()\n",
    "dimer = mdt.load(dcdfiles, top=protop, atom_indices=heavy_atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reshape the trajectory so each snapshot is a monomer, alternately chain A and B of each dimer snapshot, and then reduce down further to the catalytic domain (defined here as residues 10 - 195):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<mdtraj.Trajectory with 200000 frames, 1428 atoms, 186 residues, without unitcells>\n"
     ]
    }
   ],
   "source": [
    "monotop = dimer.topology.subset(dimer.topology.select('chainid 0'))\n",
    "monotraj = mdt.Trajectory(dimer.xyz.reshape(dimer.n_frames * 2, -1, 3), monotop)\n",
    "cd = monotraj.topology.select(cd_selection)\n",
    "cdtraj = monotraj.atom_slice(cd)\n",
    "print(cdtraj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The waymarking function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def waymark(traj, rmsd_thresh):\n",
    "    '''\n",
    "    Identify waymark structures in an MD trajectory\n",
    "    \n",
    "    Waymarks are structures selected from sequential analysis of a trajectory,\n",
    "    each of which has an rmsd from any previously selected waymark that is \n",
    "    greater than some threshold value\n",
    "    \n",
    "    '''\n",
    "    r = mdt.rmsd(traj, traj, frame=0)\n",
    "    next_waymark = np.argmax(r > rmsd_thresh)\n",
    "    rmsds = np.atleast_2d(r)\n",
    "    waymarks = [0]\n",
    "    while next_waymark > 0:\n",
    "        waymarks.append(next_waymark)\n",
    "        r = mdt.rmsd(traj, traj, frame=next_waymark)\n",
    "        rmsds = np.append(rmsds, np.atleast_2d(r), axis=0)\n",
    "        next_waymark = np.argmax(rmsds.min(axis=0) > rmsd_thresh)\n",
    "\n",
    "    assignments = rmsds.argmin(axis=0)\n",
    "    counts = [(assignments==i).sum() for i in range(len(waymarks))]\n",
    "    return waymarks, counts\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Waymark the trajectory. This cell may take a couple of minutes to run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "waymarks, counts = waymark(cdtraj, rmsd_thresh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Select waymarks having more than a threshold percentage of the trajectory snapshots in their Voronoi domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "min_frames = cdtraj.n_frames * percent_cut // 100\n",
    "chosen_waymarks = []\n",
    "chosen_counts = []\n",
    "countsum = 0\n",
    "for i, w in enumerate(waymarks):\n",
    "    if counts[i] > min_frames:\n",
    "        chosen_waymarks.append(w)\n",
    "        chosen_counts.append(counts[i])\n",
    "        countsum += counts[i]\n",
    "percent_captured = countsum * 100 // cdtraj.n_frames"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write out the waymark structures, and a log file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = open('structure_info.txt', 'w')\n",
    "f.write('PDB files for the catalytic domains of a diverse subset of\\n')\n",
    "f.write('snapshots selected from the DE Shaw 100 microsecond trajectory\\n')\n",
    "f.write('of 3CLpro.\\n\\n')\n",
    "f.write('Each structure has a heavy-atom RMSD of at least {} nm from all\\n'.format(rmsd_thresh))\n",
    "f.write('other structures and {}% of the snapshots in the trajectory\\n'.format(percent_captured))\n",
    "f.write('are within {} nm of at least one of these structures.\\n\\n'.format(rmsd_thresh))\n",
    "f.write('Each PDB file is named \"cd_<snapshot>_<chain>.pdb\" where <snapshot>\\n')\n",
    "f.write('is the index of the originating snapshot in the concatenated\\n')\n",
    "f.write('trajectory files and <chain> is the originating chain.\\n')\n",
    "f.write('The \"percent\" column gives the percentage fraction of the snapshots\\n')\n",
    "f.write('in the trajectory that are within {} nm of that structure, and \\n'.format(rmsd_thresh))\n",
    "f.write('closer to it than to any other.\\n\\n')\n",
    "f.write('   file name     percent\\n')\n",
    "\n",
    "for counts, waymark in zip(chosen_counts, chosen_waymarks):\n",
    "    if waymark % 2 == 0:\n",
    "        chain = 'A'\n",
    "    else:\n",
    "        chain = 'B'\n",
    "    pdbname = 'cd_{:06d}_{}.pdb'.format(waymark // 2, chain)\n",
    "    f.write('{}   {:4.1f}\\n'.format(pdbname, counts * 100 / 200000))\n",
    "    cdtraj[waymark].save(pdbname)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PDB files for the catalytic domains of a diverse subset of\r\n",
      "snapshots selected from the DE Shaw 100 microsecond trajectory\r\n",
      "of 3CLpro.\r\n",
      "\r\n",
      "Each structure has a heavy-atom RMSD of at least 0.2 nm from all\r\n",
      "other structures and 80% of the snapshots in the trajectory\r\n",
      "are within 0.2 nm of at least one of these structures.\r\n",
      "\r\n",
      "Each PDB file is named \"cd_<snapshot>_<chain>.pdb\" where <snapshot>\r\n",
      "is the index of the originating snapshot in the concatenated\r\n",
      "trajectory files and <chain> is the originating chain.\r\n",
      "The \"percent\" column gives the percentage fraction of the snapshots\r\n",
      "in the trajectory that are within 0.2 nm of that structure, and \r\n",
      "closer to it than to any other.\r\n",
      "\r\n",
      "   file name     percent\r\n",
      "cd_000000_A.pdb   10.6\r\n",
      "cd_000909_A.pdb    3.3\r\n",
      "cd_001104_B.pdb    5.5\r\n",
      "cd_001459_A.pdb    2.5\r\n",
      "cd_005545_B.pdb    3.4\r\n",
      "cd_006414_B.pdb    0.7\r\n",
      "cd_007728_B.pdb    3.6\r\n",
      "cd_008883_B.pdb    0.5\r\n",
      "cd_011325_B.pdb    1.6\r\n",
      "cd_014414_B.pdb    0.6\r\n",
      "cd_014821_B.pdb    0.6\r\n",
      "cd_016242_B.pdb    1.9\r\n",
      "cd_016824_A.pdb    3.7\r\n",
      "cd_018314_A.pdb    1.1\r\n",
      "cd_021119_B.pdb    0.7\r\n",
      "cd_025741_A.pdb    2.0\r\n",
      "cd_025873_B.pdb    1.1\r\n",
      "cd_026582_B.pdb    0.8\r\n",
      "cd_028103_A.pdb    1.1\r\n",
      "cd_030873_A.pdb    4.6\r\n",
      "cd_032513_B.pdb    2.1\r\n",
      "cd_039017_A.pdb    2.5\r\n",
      "cd_044370_A.pdb    3.4\r\n",
      "cd_048015_B.pdb    0.7\r\n",
      "cd_050236_A.pdb    1.2\r\n",
      "cd_054865_B.pdb    7.9\r\n",
      "cd_056256_A.pdb    1.0\r\n",
      "cd_059226_A.pdb    0.8\r\n",
      "cd_067730_A.pdb    5.2\r\n",
      "cd_070073_A.pdb    1.7\r\n",
      "cd_073746_A.pdb    0.6\r\n",
      "cd_080387_B.pdb    0.8\r\n",
      "cd_081917_B.pdb    1.8\r\n",
      "cd_090578_B.pdb    0.6\r\n"
     ]
    }
   ],
   "source": [
    "!cat structure_info.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
