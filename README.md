# Covid-19 related resources.

## Contents:

### 3CLPro: 
A Jupyter notebook that extracts a diverse subset of structures of the catalytic domain of 3CLpro from the 100,000 snapshot trajectory of the protein produced by the DE Shaw Research Group.

## Author:
[Charlie Laughton](mailto:charles.laughton@nottingham.ac.uk)
